hsearch(_, _, []).
hsearch(X, Y, [Z]) :- char(X, Y, Z).
hsearch(X, Y, [H|T]) :- char(X, Y, H), hsearch(X, Z, T), Z is Y+1.
