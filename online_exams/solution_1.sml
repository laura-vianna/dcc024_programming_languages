(*
https://www.tutorialspoint.com/execute_smlnj_online.php

*)

datatype nat = ZERO | SUCC of nat

fun nat2int(n:nat) : int = 
  case n of
    ZERO => 0
  | SUCC(m) => 1 + nat2int(m)
  
  fun add(n1:nat, n2:nat) : nat = 
  case n1 of
    ZERO => n2
  | SUCC(n_minus_1) => add(n_minus_1, SUCC(n2))
  
  fun mul(n1:nat, n2:nat) : nat =
  case n1 of
    ZERO => ZERO
  | SUCC(n1MinusOne) => add(n2, mul(n1MinusOne,n2))
  
  


