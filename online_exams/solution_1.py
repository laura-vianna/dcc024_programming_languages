# python -d solution_1.py
class Zero:
    def __init__(self):
        pass
    def __str__(self):
        return 'z'
class Succ:
    def __init__(self, n):
        self.num = n
    def __str__(self):
        return 's'+ str(self.num)


def nat2int(n):
    if isinstance(n, Zero):
        return 0;
    elif isinstance(n, Succ):
        return (1+ nat2int(n.num))
    else:
        raise Exception

def repeated(l):
    try:
        int_list = map(nat2int, l)
    except Exception:
        print "It's not an natural number!"
    else:
        return len(int_list) != len(set(int_list))

def main():
    n1 = Zero()
    n2 = Succ(Zero())
    n3 = Succ(Succ(Zero()))
    n4 = Succ(Succ(Succ(Succ(Zero()))))
    L1 = [n1, n2, n1]
    L2 = [n1, n2, n3, n4]
    L3 = [1, n2]
    print(nat2int(n1))
    print(nat2int(n2))
    print(nat2int(n3))
    print(nat2int(n4))

    print n1
    print n1, n2, n3, n4

    print(repeated(L1))
    print(repeated(L2))
    print(repeated(L3))

main()
