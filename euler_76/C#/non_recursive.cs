void Main()
{
	Partition.Run(10);
}

public class Partition {
	private static List<string> results = new List<string>();
	
    public static void Run(int n) {
        Run(n, n, "");
		
		// Pretty output
		results.ForEach(x => Console.WriteLine(string.Join(" + ", x.TrimStart().Split(' '))));
	}
	
    public static void Run(int n, int max, string prefix) {
        if (n == 0) {
            results.Add(prefix);
            return;
        }

        for (int i = Math.Min(max, n); i >= 1; i--) {
            Run(n-i, i, prefix + " " + i);
        }
    }
}
