# void Main()
# {
# 	var number = 10;
#     recurse(number, 1, 0, "");
#
# 	Console.WriteLine($"Number of results = {results.Count()}");
# }
# int recurse(int n, int min, int count, string result)
# {
# 	if (n == 0)
# 	{
# 		// Add it to the list in a nice format so we can view it if we want
#   		results.Add(string.Join(" + ", result.TrimStart().Split(' ')));
# 	}
#
# 	for (int x = min ; x <= n ; x++) {
#   		count = recurse(n - x, x, count, result + " " + x);
#   	}
#
#   	return count;
# }

# def euler_76(n,min,count):
def euler_76(n,min,count,result):
    if n == 0:
        print '+'.join(str(e) for e in result)
    for x in range(min,n+1):
        count = euler_76(n-x, x, count, (result+[x]))

    return count

def main():
    resul = []
    euler_76(2,1,0,resul)
    euler_76(3,1,0,resul)
    euler_76(4,1,0,resul)
    euler_76(5,1,0,resul)

main()
